const wallColor = "#777", cellColor = "#BBB", pathColor = "#0F0", playerColor = "#00F", goalColor = "#F00", winColor = "#FF0", directions = [ {x: 1, y: 0}, {x: 0, y: 1}, {x: -1, y: 0}, {x: 0, y: -1} ];
var width, height, wallSize, cellSize, pathSize, endpointSize, delay;
var start, goal;
var canvas, ctx, maze, stack, pos, timeout;

function setup() {
	if (typeof timeout != 'undefined') clearTimeout(timeout);
	document.onkeypress = null;
	
	width = getInput("width");
	height = getInput("height");
	wallSize = getInput("wallSize");
	cellSize = getInput("cellSize");
	pathSize = getInput("pathSize");
	endpointSize = getInput("endpointSize");
	delay = getInput("delay");
	
	start = { x: 0, y: height - 1 };
	goal = { x: width - 1, y: 0 };
	
	canvas = document.getElementById("MazeCanvas");
	ctx = canvas.getContext("2d");
	
	canvas.width = (width * cellSize) + ((width+1) * wallSize);
	canvas.height = (height * cellSize) + ((height+1) * wallSize);
	
	rect(0, 0, canvas.width, canvas.height, wallColor);
	
	maze = [];
	for (var x = 0; x < height; x++) {
		maze[x] = [];
		for (var y = 0; y < width; y++) {
			maze[x][y] = 0;
		}
	}
	
	stack = [pos = goal];
	drawCell(pos.x, pos.y, cellSize, cellColor);
	drawEnd(pathColor);
	generate(function() {
		stack = [pos = start];
		drawEnd(playerColor);
		drawCell(goal.x, goal.y, endpointSize, goalColor);
		document.onkeypress = function(e) {
			switch(e.keyCode) {
				case 39:
					if (maze[pos.x][pos.y] & 1) moveStack(playerColor, 0);
					break;
				case 40:
					if (maze[pos.x][pos.y] & 2) moveStack(playerColor, 1);
					break;
				case 37:
					if (maze[pos.x][pos.y] & 4) moveStack(playerColor, 2);
					break;
				case 38:
					if (maze[pos.x][pos.y] & 8) moveStack(playerColor, 3);
					break;
			}
			if (pos.x == goal.x && pos.y == goal.y) {
				document.onkeypress = null;
				drawCell(start.x, start.y, pathSize, winColor);
				drawEnd(cellColor);
				var tempStack = stack;
				stack = [pos = start];
				for (var i = 1; i < tempStack.length; i++) {
					stack[i] = pos = tempStack[i];
					joinCell(winColor, pathSize);
				}
			}
		}
	});
}

function generate(callback) {
	var direction = [];
	for (var d = 0; d < 4; d++) if (cellMovable(move(pos, d))) direction.push(d);
	if (direction.length > 0) {
		var direction = direction[Math.floor(Math.random() * direction.length)];
		maze[pos.x][pos.y] += 1 << direction;
		forward(pathColor, direction);
		maze[pos.x][pos.y] += 1 << ((direction + 2) % 4);
	}
	else if (back(pathColor)) {
		callback();
		return;
	}
	
	if (delay == 0) generate(callback);
	else timeout = setTimeout(function() { generate(callback) }, delay);
}

function moveStack(color, direction) {
	var last = stack[stack.length - 2];
	var movement = directions[direction];
	if (last && movement.x == last.x - pos.x && movement.y == last.y - pos.y) back(color);
	else forward(color, direction);
}

function forward(color, direction) {
	drawEnd(cellColor);
	if (stack.length > 1) joinCell(color, pathSize);
	else drawCell(pos.x, pos.y, pathSize, color);
	pos = move(pos, direction);
	stack.push(pos);
	joinCell(cellColor, cellSize);
	joinCell(color, pathSize);
	drawEnd(color);
}

function back(color) {
	if (stack.length > 1) joinCell(cellColor, endpointSize);
	stack.pop();
	pos = stack[stack.length - 1];
	if (stack.length == 0) return true;
	else {
		drawEnd(color);
		return false;
	}
}

function drawEnd(color) {
	drawCell(pos.x, pos.y, endpointSize, color);
}

function joinCell(color, size) {
	var last = stack[stack.length - 2];
	var width = pos.x - last.x, height = pos.y - last.y;
	var startCell = cellPos(last.x, last.y);
	if (width != 0) rect(startCell.x + (cellSize / 2) + (width * size / 2), startCell.y + ((cellSize - size) / 2), width * (cellSize + wallSize), size, color);
	if (height != 0) rect(startCell.x + ((cellSize - size) / 2), startCell.y + (cellSize / 2) + (height * size / 2), size, height * (cellSize + wallSize), color);
}

function drawCell(x, y, size, color) {
	var startCell = cellPos(x, y);
	rect(startCell.x + ((cellSize - size) / 2), startCell.y + ((cellSize - size) / 2), size, size, color);
}

function move(point, dir) {
	return { x: point.x + directions[dir].x, y: point.y + directions[dir].y };
}

function cellMovable(cell) {
	return cell.x >= 0 && cell.x < width && cell.y >= 0 && cell.y < height && maze[cell.x][cell.y] == 0;
}

function cellPos(x, y) {
	return { x: (x * cellSize) + ((x+1) * wallSize), y: (y * cellSize) + ((y+1) * wallSize)};
}

function rect(x, y, width, height, color) {
	if (width < 0) { x += width; width = -width; }
	if (height < 0) { y += height; height = -height; }
	ctx.fillStyle = color;
	ctx.fillRect(x, y, width, height);
}

function getInput(id) {
	var elemnt = document.getElementById(id);
	if (elemnt.type = "number") return parseInt(elemnt.value);
	else return elemnt.value;
}

document.getElementById("generate").onclick = setup;